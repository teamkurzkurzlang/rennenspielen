package Rennschnecken;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Rennen {

    static int newstep() {
        Random random = new Random();
        int max = 10;
        int min = 1;
        int totalNumber = 1;


        int test = random.nextInt(max - min + 1) +min;
        //stream.forEach(System.out::println);
        return test;
    }

    static int[] rankPlayers ( int[] array){
        //calcs out positions
        HashMap<Integer, Integer> m = new HashMap <>();
        for (int i = 0; i < array.length; i++) {
            m.put(i, array[i]);
        }
        return m.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .mapToInt(Map.Entry::getKey)
                .toArray();
    }
    public static int[] rankify(int A[], int n)
    {
        // Rank Vector
        int R[] = new int[n];

        // Sweep through all elements in A
        // for each element count the number
        // of less than and equal elements
        // separately in r and s
        for (int i = 0; i < n; i++) {
            int r = 1, s = 1;

            for (int j = 0; j < n; j++)
            {
                if (j != i && A[j] > A[i])
                    r += 1;

                if (j != i && A[j] == A[i])
                    s += 1;
            }
            // Use formula to obtain rank
            R[i] = r + (int)(s - 1) / (int) 2;

        }
        return R;
    }
    // Method for getting the maximum value
    @Contract(pure = true)
    public static int getMax(int[] inputArray){
        int maxValue = inputArray[0];
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }
    @Contract(pure = true)
    public static boolean twoLeaders(int[] positions) {
        boolean duplicates;
        duplicates=false;
        for (int j=0;j<positions.length;j++)
            for (int k=j+1;k<positions.length;k++)
                if (k!=j && positions[k] == positions[j] && positions[k] == 1)
                    duplicates=true;

        return duplicates;
    }
    public static void showPositions(@NotNull int[] positions) {
            for (int i=0; i<positions.length; i++) {
                System.out.println("Position " + positions[i]);
            }
    }
    public static void showDistance(@NotNull int[] distance) {
        for (int i=0; i<distance.length; i++) {
            System.out.println("Distance " + distance[i]);
        }
    }

}
