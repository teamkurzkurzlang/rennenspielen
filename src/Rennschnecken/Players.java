package Rennschnecken;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;

public class Players {
    String playername;
    int koordinate = 0;
    int koordinateaAlt = 0;
    int position = 0;
    boolean isNPC;
    static String username1;
    static String username2;

    ArrayList<Players> alPlayers = new ArrayList <>();
    ArrayList<String> temp = new ArrayList <>();

    public Players(){}
    public Players(String name) {
        this.playername = name;
    }
    void setDistance( int koordinate){
        this.koordinate = this.koordinate + koordinate;
    }
    void setPosition(int position) {
        this.position = position;
    }

    int getKoordiante() {
        return koordinate;
    }

    public void setUsername(String username, int number) {
        if (number == 1) {
            username1 = username;
        }
        if (number == 2) {
            username2 = username;
        }
    }
    public String getUsername1() {
        return username1;
    }
    @Contract(pure = true)
    public static String getUser() {
        return "User: " + "\"" + username1 + "\"" +" created";
    }

    @Override
    public String toString() {
        if (isNPC) {
            return playername + " Fortschritt: " + koordinate + " aktueller Platz: " + position;
        }
        else {
            return "User: " + "\"" + playername + "\"" + " Fortschritt: " + koordinate + " aktueller Platz: " + position;
        }
    }
}
