package Rennschnecken;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Helper extends Rennen {

    ArrayList<Players> alPlayerObjects = new ArrayList <>();
    int playerCount;
    static int maxDistance;
    int[] distancePlayers = new int[6];
    int sizeDistanceArray = distancePlayers.length;
    int[] arrayPositions = new int[distancePlayers.length];
    int leadingPlayer;

    public Helper() {

    }
    public Helper(int playerCount) {
        this.playerCount = playerCount;
    }

    public void loadPlayers(ArrayList<String> alPlayerNames) {

        //load names in Objects
        for (int i=0; i<playerCount; i++) {
            String tempString = alPlayerNames.get(i);
            alPlayerObjects.add(i, new Players(tempString));
            //get 3 first chars look for NPC, get playername from Players Objects, detect if it is a User
            int lenght = 3;
            String tempShort = tempString.substring(0, Math.min(tempString.length(), lenght));
            if (tempShort.equals("NPC")) {
                alPlayerObjects.get(i).isNPC = true;
            }
            else {
                alPlayerObjects.get(i).isNPC = false;
            }
        }
    }
   public void raceRound() {
        for (int i=0; i<alPlayerObjects.size(); i++) {
            int temp = Rennen.newstep();
            //fill and check distance Array
            alPlayerObjects.get(i).setDistance(temp);
            distancePlayers[i] = alPlayerObjects.get(i).getKoordiante();
        }
        //set Positions for Objects, and fill temp arrayPositions array
       for (int i=0; i<alPlayerObjects.size(); i++) {
           arrayPositions = Rennen.rankify(distancePlayers, sizeDistanceArray);
           alPlayerObjects.get(i).setPosition(arrayPositions[i]);
       }
       // set leading player
       for (int y=0; y<distancePlayers.length; y++) {
           if (arrayPositions[y] == 1 && !Rennen.twoLeaders(arrayPositions)) { //toDo: react on twoWinners
               leadingPlayer = y;
           }
       }
    }
    public void showProgress(int round) {
        for (int i=0; i<alPlayerObjects.size(); i++) {
            System.out.println("Stand nach Runde: " + round + " " + alPlayerObjects.get(i).toString());
        }
        System.out.println("------------------------------------------------------");
     }
    public String showProgressForClient(int round, int player) {
        String output;
            output = ("Stand nach Runde: " + round + " " + alPlayerObjects.get(player).toString());

        return output;
    }
     public int getMaxDistance() {
         maxDistance = Rennen.getMax(distancePlayers);
         return  maxDistance;
     }
     public int[] getRank() {
         return arrayPositions;
     }
     public void showWinner() throws InterruptedException {
         if (Rennen.twoLeaders(arrayPositions)) {
             System.out.println("two winners, rematch");
             TimeUnit.SECONDS.sleep(1);
         }
         else {
             System.out.println("Waiting for Race to finish");
             TimeUnit.SECONDS.sleep(1);
             System.out.println((char) 27 + "[31m" + "Gewinner: " + alPlayerObjects.get(leadingPlayer).toString());
         }
     }
    public String showWinnerForClient() throws InterruptedException {
        String output;
        if (Rennen.twoLeaders(arrayPositions)) {
            output = ("two winners, rematch");
            TimeUnit.SECONDS.sleep(1);
        }
        else {
            System.out.println("Waiting for Race to finish");
            TimeUnit.SECONDS.sleep(1);
            output = ((char) 27 + "[31m" + "Gewinner: " + alPlayerObjects.get(leadingPlayer).toString());
        }
        return output;
    }

}
