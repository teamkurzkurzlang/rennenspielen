package Rennschnecken.NET;

import Rennschnecken.Helper;
import Rennschnecken.Main;

import java.io.*;
import java.net.*;

public class Client {
    public static void main(String[] args) throws IOException {

        Main game1 = new Main();
        String hostName = "localhost";
        int portNumber = 44000;
        boolean gameRunning;
        Helper h1 = new Helper();

        try (
                Socket kkSocket = new Socket(hostName, portNumber);
                PrintWriter toServerSoc = new PrintWriter(kkSocket.getOutputStream(), true);
                BufferedReader fromServerSoc = new BufferedReader(
                        new InputStreamReader(kkSocket.getInputStream()));
        ) {
            BufferedReader stdIn =
                    new BufferedReader(new InputStreamReader(System.in));

            gameRunning = true; //toDO get game end

            String fromServer;
            String fromUser;

            //while ((fromServer = in.readLine()) != null) {
            fromServer = fromServerSoc.readLine();
            System.out.println("Server: " + fromServer);
            fromServer = fromServerSoc.readLine();
            System.out.println("Server " + fromServer);
            fromUser = stdIn.readLine();
            if (fromUser != null) {
                //give fromuser to Server
                toServerSoc.println(fromUser);
            }
            fromServer = fromServerSoc.readLine();
            System.out.println(fromServer);
            //while (gameRunning == true) {
            System.out.println("How many Players you want?");
            fromUser = stdIn.readLine();
            toServerSoc.println(fromUser);
            fromServer = fromServerSoc.readLine();
            System.out.println(fromServer);
            int i=0;
            while (fromServer != null) {
                fromServer = fromServerSoc.readLine();
                System.out.println(fromServer);
                i++;
            //gameRunning = false;
            }
            fromServer = fromServerSoc.readLine();
            System.out.println(fromServer);

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    hostName);
            System.exit(1);
        } /*catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
}