package Rennschnecken.NET;

import Rennschnecken.Helper;
import Rennschnecken.Main;
import Rennschnecken.Players;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Cloneable{

    public static void main(String[] args) throws IOException {
        Main game = new Main();
        int portNumber = 44000;
        String inputLine, outputLine;
        boolean gameRunning = true;

        Players p1 = new Players();
        ArrayList<String> alPlayernames = new ArrayList <>();

        String username2 = "user2";
         String username1 = null;
         String npc1 = "NPC Lari";
         String npc2 = "NPC Herbert";
         String npc3 = "NPC Lui";
         String npc4 = "NPC Salvatore";
         String npc5 = "NPC Schweinchen Lisa";
         String npc6 = "NPC Fari";
         String npc7 = "NPC FlyLo";

        System.out.println("Server up and running, listening on Port: " + portNumber);
        try (
                ServerSocket serverSocket = new ServerSocket(portNumber);
                Socket clientSocket = serverSocket.accept();
                PrintWriter toClientSoc =
                        new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader fromClientSoc = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
        ) {

            // Initiate conversation with client
            Protocol kkp = new Protocol();
            //KnockKnockProtocol kkp = new KnockKnockProtocol();
                outputLine = kkp.welcome(null);
                toClientSoc.println(outputLine);
                //out is the Writer to client socket
                //out.println(outputLine);
                //game.main(args);
                inputLine = fromClientSoc.readLine();

                    //get input Stream from Client
                    //set Username to tempArray
                    //load mames in array  toDO
                    username1 = inputLine;
                     if (username1 == null) {
                        username1 = "User1Empty";
                        }
                    alPlayernames.add(username1);
                    alPlayernames.add(username2);
                    alPlayernames.add(npc1);
                    alPlayernames.add(npc2);
                    alPlayernames.add(npc3);
                    alPlayernames.add(npc4);
                    alPlayernames.add(npc5);
                    alPlayernames.add(npc6);
                    alPlayernames.add(npc7);
                    outputLine = kkp.setUsername(inputLine);
                    toClientSoc.println(outputLine);

                //get Player Numbers, parse input to int
                int numberPLayers = Integer.parseInt(fromClientSoc.readLine());
                //int anzahl = inputLine;
                outputLine = "Starting game... with " + numberPLayers + " Players";
                toClientSoc.println(outputLine);

                int maxDistanceNPCs;
                Helper h1 = new Helper(numberPLayers);
                h1.loadPlayers(alPlayernames);
                int i = 1;
                do {
                    h1.raceRound();
                    for (int ii=0; ii<numberPLayers; ii++) {
                        outputLine = h1.showProgressForClient(i, ii);
                        //h1.showProgressForClient(i, ii);
                        //h1.showProgress(i);
                        toClientSoc.println(outputLine);
                    }
                    maxDistanceNPCs = h1.getMaxDistance();
                    outputLine = ("-------------------------------");
                    toClientSoc.println(outputLine);
                    i++;
                }while (maxDistanceNPCs<100);

                outputLine = h1.showWinnerForClient();
                System.out.println(outputLine);
                toClientSoc.println(outputLine);

            System.out.println("restarting server socket");
                serverSocket.close();
                main(args);
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                    + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        } /*catch (InterruptedException e) {
            e.printStackTrace();
        }*/ catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
