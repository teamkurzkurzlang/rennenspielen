package Rennschnecken;

import Rennschnecken.NET.Client;
import Rennschnecken.NET.Server;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        /** Interface
         System.out.println("Username: ");
         Scanner input = new Scanner(System.in);
         String username = input.next();
         System.out.println("Schnecken Name: ");
         String schneckenName = input.next();
         System.out.println("Anzahl Gegner (Max 5): ");
         String anzahl = input.next();
         input.close();
         **/
        int anzahlNPCs = 4;
        int anzahlUser = 2;
        int maxDistanceNPCs = 0;
        int[] distanceNPCs = new int[anzahlNPCs + anzahlUser];
        int distanceUser[] = new int[anzahlNPCs + anzahlUser];
        int ii = 0;

        String username1;
        String username2 = "user2";
        String npc1 = "NPC Lari";
        String npc2 = "NPC Herbert";
        String npc3 = "NPC Lui";          // Helper Class für Array usw. Objekte aus den Namen machen
        String npc4 = "NPC Salvatore";
        String npc5 = "NPC Schweinchen Lisa";
        String npc6 = "NPC Fari";

        //create Objects
        Rennen r1 = new Rennen();
        Players p1 = new Players();
        Client c1 = new Client();
        Server s1 = new Server();
        Helper h1 = new Helper(anzahlNPCs + anzahlUser);
        // init distance array Position 0 is User, rest NPCs
        username1 = p1.getUsername1();
        if (username1 == null) {
            username1 = "User1Empty";
        }


        ArrayList <String> alPlayernames = new ArrayList <>();
        //ArrayLists for Players Objects
        ArrayList <Players> alPlayerObjects = new ArrayList <>();


        //load mames in array  toDO
        alPlayernames.add(username1);
        alPlayernames.add(username2);
        alPlayernames.add(npc1);
        alPlayernames.add(npc2);
        alPlayernames.add(npc3);
        alPlayernames.add(npc4);
        alPlayernames.add(npc5);
        alPlayernames.add(npc6);

        h1.loadPlayers(alPlayernames);
        int i = 1;
        do {
            h1.raceRound();
            h1.showProgress(i);
            h1.getRank();

            maxDistanceNPCs = h1.getMaxDistance();
            i++;
    }while (maxDistanceNPCs<100);
        h1.showWinner();
  }
}
